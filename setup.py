from setuptools import setup, find_packages

name = 'PyCS'
version = '1.0.0'
author = 'Jonas Harsch'
author_email = 'jonas.harsch@inm.uni-stuttgart.de'
url = ''
description = 'Python package for computing numerical derivatives including \
               complex step (CS) differentiation.'
long_description = ''
license ='LICENSE'

setup(
    name=name,
    version=version,
    author=author,
    author_email=author_email,
    url=url,
    description=description,
    long_description=long_description,
    license=license,
    install_requires=[
        'numpy>=1.21.3', 
        'scipy>=1.7.1', 
        'matplotlib>=3.4.3', 
        ],
    packages=find_packages(),
    python_requires='>=3.7',
)