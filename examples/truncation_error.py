import numpy as np
from PyCS.math import approx_fprime
from scipy.optimize._numdiff import approx_derivative
import matplotlib.pyplot as plt


if __name__ == '__main__':
    # Complex Step Differentiation example from 
    # https://blogs.mathworks.com/cleve/2013/10/14/complex-step-differentiation/
    def f(x):
        return np.exp(x) / (np.sin(x)**3 + np.cos(x)**3)

    def f_x(x):
        den = np.sin(x)**3 + np.cos(x)**3
        return np.exp(x) * (1.0 / den - (3 * np.cos(x) * np.sin(x)**2 - 3 * np.sin(x) * np.cos(x)**2) / den**2)

    x0 = np.pi / 4

    num = 500
    eps = np.power(10.0, -np.linspace(1, 16, num=num))
    err = np.nan * np.ones((num, 6))
    for i in range(num):
        err[i, 0] = np.abs(approx_fprime(x0, f, eps=eps[i], method='2-point') - f_x(x0))
        err[i, 1] = np.abs(approx_fprime(x0, f, eps=eps[i], method='3-point') - f_x(x0))
        err[i, 2] = np.abs(approx_fprime(x0, f, eps=eps[i], method='cs') - f_x(x0))
        err[i, 3] = np.abs(approx_derivative(f, x0, rel_step=eps[i], 
                                             abs_step=eps[i], method='2-point') - f_x(x0))
        err[i, 4] = np.abs(approx_derivative(f, x0, rel_step=eps[i], 
                                             abs_step=eps[i], method='3-point') - f_x(x0))
        err[i, 5] = np.abs(approx_derivative(f, x0, rel_step=eps[i], 
                                             abs_step=eps[i], method='cs') - f_x(x0))

    fig, ax = plt.subplots()
    ax.loglog(eps, err[:, 0], label='2-point')
    ax.loglog(eps, err[:, 1], label='3-point')
    ax.loglog(eps, err[:, 2], label='cs')
    ax.loglog(eps, err[:, 3], '--', label='scipy 2-point')
    ax.loglog(eps, err[:, 4], '--', label='scipy 3-point')
    ax.loglog(eps, err[:, 5], '--', label='scipy cs')
    ax.invert_xaxis()
    ax.set_xlabel('eps')
    ax.set_ylabel('error')
    ax.legend()
    ax.grid()
    plt.show()