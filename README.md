PyCS
====
Python package for computing numerical derivatives including complex step (CS) differentiation for demonstration of python pip's *editable mode*.

Installation
------------

Typically we install a python package via

```bash
python -m pip install .
```

During development a more convenient way is installing a package via pip's [*editable mode*](https://pip.pypa.io/en/stable/cli/pip_install/#cmdoption-e), i.e., setuptools *develop mode*

```bash
python -m pip install -e .
python -m pip install --editable .
```

Such an installation recognizes changes in the library immediately and even works with python's debugger.

Example
-------

The example `truncation_error.py` uses the `approx_fprime` function of the python module `PyCS`. It is inspired by a [MathWorks blog post](https://blogs.mathworks.com/cleve/2013/10/14/complex-step-differentiation/). After installing the package via 

```bash
python -m pip install -e .
```

changes made to the function, e.g. adding an additional line

```python
print(f'approx_fprime called with x0: {x0}')
```

are recoginzed immediately.